<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE Autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $id_a = $_POST['id'];
  $nombre_a = $_POST['nombre'];

  if (empty($id_a)) {
    $error = true
?>
      <p>Error, no se indico el ID_AUTOR</p>
<?php
  }
  if (empty($nombre_a)) {
    $error = true;
?>
      <p>Error, no se indico el nombre del autor</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "insert into biblioteca.autor (id_autor,nombre_autor) values ('".$id_a."','".$nombre_a."');";
    $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
    if (pg_affected_rows($resultado) == 0) {
?>
      <p>Error al momento de guardar los datos del autor</p>
<?php
    } 
    else {
?>
      <p>El autor con id <?php echo $id_a; ?> y nombre "<?php echo $nombre_a; ?>" ha sido guardado con exito.</p>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-autor.php">Nuevo Autor</a></li>
</ul>

</body>
</html>