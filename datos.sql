set search_path to biblioteca, public;


INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Buena','6987714554');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('2','Con hojas rayadas','6987714554');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Faltan hojas','6874596184');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Pesima','5221695172');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Buena','1534359937');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Buena','6875362773');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('2','Pesima','6875362773');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Faltan hojas','2339525568');
INSERT INTO ejemplar (clave_ejemplar, conservacion_ejemplar, isbn) VALUES ('1','Sin portadas','2339525568');
