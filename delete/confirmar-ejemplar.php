<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  $isbn = $_GET['isbn'];
  $conser = $_GET['conser'];
  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se ha indicado el clave_ejemplar del ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."' and conservacion_ejemplar='".$conser."' and isbn='".$isbn."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con clave_ejemplar <?php echo $clave_ejemplar; ?></p>
<?php
    } 
    else {
        $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);    	  
?>
<table>
  <caption>Información del Ejemplar</caption>
  <tbody>
    <tr>
      <th>clave_ejemplar</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>Conservacion Ejemplar</th>
      <td><?php echo $conser; ?></td>
    </tr>
	<tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
    <tr>
      <th>Libro/s</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro as L
        inner join biblioteca.ejemplar as E
          on (L.isbn = E.isbn and L.isbn = '".$isbn."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin Libro</p>
<?php
      } 
      else 
      {
        $tupla = pg_fetch_array($autores, null, PGSQL_ASSOC);
?>
        <?php echo $tupla['titulo_libro']; ?>
<?php
      }
?>
    </tr>
    <tr>
      <th>Autore/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.autor as A inner join biblioteca.libro_autor as 
        LA on LA.id_autor=A.id_autor
        where isbn = '".$isbn."';";

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin Autor/es</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplares, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
            <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" />
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <p>¿Está seguro/a de eliminar este ejemplar?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los ejemplares en existencia, así como la relación
    que mantenga con sus autores.
  </p>
</form>

<form action="ejemplar.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplar.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
