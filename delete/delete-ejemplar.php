<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $clave_ejemplar = $_POST['clave_ejemplar'];
  $isbn = $_POST['isbn'];
  $error = false;
  if (empty($clave_ejemplar)) {
    $error = true;
?>
    <p>Error, no se indico La clave del Ejemplar</p>
<?php 
  }
  if(empty($isbn)){
    $error = true;
?>
    <p>Error, no se indico el ISBN del Ejemplar</p>
<?php
  }
  if(!$error){
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar, isbn
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."' and isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con la Clave <?php echo $clave_ejemplar; ?></p>
<?php
    } 
    else {
      $query = "delete from biblioteca.ejemplar where clave_ejemplar = '".$clave_ejemplar."' and isbn='".$isbn."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar el Ejemplar</p>
<?php
      } 
      else {
?>
        <p>El Ejemplar con la clave <?php echo $clave_ejemplar; ?> fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplar.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
