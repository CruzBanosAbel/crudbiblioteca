<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $clave_ejemplar = $_POST['clave_ejemplar'];
  $conservacion_ejemplar = $_POST['conservacion_ejemplar'];
  $isbn = $_POST['isbn'];

  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico la clave del ejemplar</p>
<?php
  }
  if (empty($conservacion_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico la conservacion del ejemplar</p>
 <?php
  }
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con esta Clave  <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $query = "update biblioteca.ejemplar
        set conservacion_ejemplar = '".$conservacion_ejemplar."', 
		    isbn = '".$isbn."' where clave_ejemplar = '".$clave_ejemplar."' and isbn = '".$isbn."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del Ejemplar</p>
<?php
      } else {
?>
  <p>Los datos del Ejemplar con Clave <?php echo $clave_ejemplar; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplar.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
